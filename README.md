## Welcome to the repo for my Figma API tutorial.

## Installation.
Clone this repo to your computer and run `npm install` in both the root directory and inside the `client` folder.
You'll also need to add your own API keys + File IDs to `server.js` in the root.
After you have done that you'll need to run `npm start` in both the root and the `client` folder to start the two servers.

### Feedback
If you spot any mistakes with the repo OR the tutorials, let me know with an issue here or a response to the tutorials.

